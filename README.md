## Clone repository via https
```sh
git clone https://gitlab.com/vooidka/frequency-vocabulary.git
cd frequency-vocabulary/
```

## Installation

```sh
mkdir .build
cd .build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$nrpoc
make install
```

## Usage
```sh
./out/bin/freq_vocab --in ./out/bin/in.txt --out ./out.txt
```
