#include <getopt.h>
#include <iostream>
#include <memory>
#include "FrequencyVocabulary.hh"

void PrintUsage(int argc, char *argv[])
{
    std::cout << "freq_vocab \t--in inputfile.txt --out outfile.txt\n";
    std::cout << "freq_vocab \t--help"
              << " print this message" << std::endl;
}

bool ParseCommandLineOptions(int argc, char *argv[], std::string &in, std::string &out)
{
    /// NOTE: Here POPT lib can be used
    char ch;
    static struct option long_options[] = {{"help", no_argument, 0, 'h'},
                                           {"in", required_argument, 0, 'i'},
                                           {"out", required_argument, 0, 'o'},
                                           {nullptr, 0, nullptr, 0}};

    while ((ch = getopt_long(argc, argv, "h:i:o", long_options, NULL)) != -1) {
        switch (ch) {
            case 'h':
                PrintUsage(argc, argv);
                break;

            case 'i':
                if (optarg) {
                    in = std::string(optarg);  // not necessary to copy here
                }
                break;

            case 'o':
                if (optarg) {
                    out = std::string(optarg);  // not necessary to copy here
                }
                break;

            default:
                PrintUsage(argc, argv);
                return false;
        }
    }
    if (in.empty()) {
        std::cout << "option --in <file.txt> is mandatory" << std::endl;
        return false;
    }

    if (out.empty()) {
        out = "./out.txt";
        std::cout << "option --out is empty. ./out.txt will be used" << std::endl;
    }

    return true;
}

int main(int argc, char *argv[])
{
    std::string infile;
    std::string outfile;
    if (!ParseCommandLineOptions(argc, argv, infile, outfile)) {
        return EXIT_FAILURE;
    }
    std::unique_ptr<CFrequencyVocabulary> fv =
        std::make_unique<CFrequencyVocabulary>(infile, outfile);
    fv->Process();
    fv->Write();

    return 0;
}
