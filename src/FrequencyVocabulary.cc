#include "FrequencyVocabulary.hh"
#include <algorithm>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include "LookupTable.hh"

CFrequencyVocabulary::CFrequencyVocabulary(std::string &infile, std::string &outfile)
    : infile_(infile), outfile_(outfile)
{
}

void CFrequencyVocabulary::Construct()
{
    freq_vocab_.reserve(freq_map_.size());
    for (auto &kv : freq_map_) {
        freq_vocab_.emplace_back(kv.second, std::move(kv.first));
    }
}

void CFrequencyVocabulary::Sort()
{
    auto cmp = [&](std::pair<size_t, std::string> const &a,
                   std::pair<size_t, std::string> const &b) {
        if (a.first == b.first) {
            return a.second < b.second;
        }
        return freq_map_[a.second] > freq_map_[b.second];
    };
    std::sort(freq_vocab_.begin(), freq_vocab_.end(), cmp);
}

void CFrequencyVocabulary::Process() noexcept
{
    std::ifstream file;
    file.open(infile_.c_str());
    if (!file.is_open()) {
        std::cerr << "An error has been occured during open the file " << infile_ << ";"
                  << std::strerror(errno);
        return;
    }

    try {
        std::string word;
        while (file >> word) {
            std::transform(word.begin(), word.end(), word.begin(),
                           [&](unsigned char x) { return table[x]; });
            word.erase(
                std::remove_if(word.begin(), word.end(), [&](unsigned char c) { return c == ' '; }),
                word.end());
            freq_map_[word]++;
        }
        file.close();  /// not necessary

        Construct();
        Sort();
    } catch (std::exception &e) {
        std::cerr << "Error has been occured during compute frequency vocabulary '" << e.what()
                  << "'";
    }
}

std::vector<std::pair<size_t, std::string>> CFrequencyVocabulary::Get()
{
    return freq_vocab_;
}

void CFrequencyVocabulary::Write() noexcept
{
    try {
        std::ofstream out(outfile_.c_str());
        for (const auto &e : freq_vocab_) {
            out << e.first << " '" << e.second << "'\n";
        }
        out.close();  /// not necessary
    } catch (std::exception &e) {
        std::cerr << "An error has been occured during write to '" << outfile_ << "'; " << e.what();
    }
}
