#ifndef FREQUENCYVOCABULARY_H
#define FREQUENCYVOCABULARY_H

#include <string>
#include <unordered_map>
#include <vector>

/**
 * @class CFrequencyVocabulary
 * @brief CFrequencyVocabulary construct frequency vocabulary of infile
 *        and store result in outfile
 */
class CFrequencyVocabulary {
  private:
    /// input file for analyse
    std::string infile_;

    /// outfilt for store result
    std::string outfile_;

    /// frequency map, construct during reading input file
    std::unordered_map<std::string, size_t> freq_map_;

    /// sorted container first by frequency, second by alphabet
    std::vector<std::pair<size_t, std::string>> freq_vocab_;

    /**
     * @brief  Construct list of pairs[frequency, word]
     * @details Iterate over std::map and store [k, v] as [v, k] in std::list
     * @return void
     */
    void Construct();

    /**
     * @brief   sort list of pairs by pair.first argument
     * @return void
     */
    void Sort();

  public:
    CFrequencyVocabulary(std::string &infile, std::string &outfile);
    ~CFrequencyVocabulary() = default;

    explicit CFrequencyVocabulary(const CFrequencyVocabulary &) = delete;
    CFrequencyVocabulary(CFrequencyVocabulary &&) = delete;
    CFrequencyVocabulary &operator=(const CFrequencyVocabulary &) = delete;
    CFrequencyVocabulary &operator=(CFrequencyVocabulary &&) = delete;

    /**
     * @brief construct frequency vocabulary
     * @detail open infile, read word by word, strip word to a-zA-Z,
     *         store word and frequency in std::map.
     *         After, conver map[k,v] to list<pair<v,k>>
     *         and stable sort by 'v'
     * @return void
     */
    void Process() noexcept;

    /**
     * @brief Write list<pair> to outfile
     * @return void
     */
    void Write() noexcept;

    std::vector<std::pair<size_t, std::string>> Get();
};

#endif  // FREQUENCYVOCABULARY_H
